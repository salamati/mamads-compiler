grammar Acton;

// TODO: ternary operator

//Punctuations
DOT:
    '.'
    ;

COMMA:
    ','
    ;

COLON:
    ':'
    ;

SEMICOLON:
    ';'
    ;

RCURLY:
    '}'
    ;

LCURLY:
    '{'
    ;

RBRACKET:
    ']'
    ;

LBRACKET:
    '['
    ;

RPAR:
    ')'
    ;

LPAR:
    '('
    ;

QUESTIONMARK:
    '?'
    ;

COMMENT:
    '//'
    ;

//Operators
EQUALS:
    '=='
    ;

NOTEQUALS:
    '!='
    ;

GREATER:
    '>'
    ;

LESSER:
    '<'
    ;

NOT:
    '!'
    ;

LOGICALAND:
    '&&'
    ;

LOGICALOR:
    '||'
    ;

INCREMENT:
    '++'
    ;

DECREMENT:
    '--'
    ;

PLUS:
    '+'
    ;

MINUS:
    '-'
    ;

MULTIPLICATION:
    '*'
    ;

DIVIDE:
    '/'
    ;

MOD:
    '%'
    ;

ASSIGNMENT:
    '='
    ;

// Keyword
MSGHANDLER:
    'msghandler'
     ;

INITIAL:
    'initial'
    ;

EXTENDS:
    'extends'
    ;

ACTORVARS:
    'actorvars'
    ;

KNOWNACTORS:
    'knownactors'
    ;

ACTOR:
    'actor'
    ;

PRINT:
    'print'
    ;

FOR:
    'for'
    ;

ELSE:
    'else'
    ;

IF:
    'if'
    ;

SENDER:
    'sender'
    ;

SELF:
    'self'
    ;

MAIN:
    'main'
    ;

STRING:
    'string'
    ;

BOOLEAN:
    'boolean'
    ;

INT:
    'int'
    ;

TRUE:
    'true'
    ;

FALSE:
    'false'
    ;

CONTINUE:
    'continue'
    ;

BREAK:
    'break'
    ;

// Others
LITERAL:
    '"'~["]*'"'
    ;

NUMBER:
    [1-9][0-9]* | '0'
    ;

IDENTIFIER:
    [a-zA-Z_][a-zA-Z0-9_]*
    ;

WS:
    [ \t\r\n] -> skip
    ;

COMMENTPHRASE:
    COMMENT ~[\n]* -> skip
    ;


// <---------------- actor ---------------->


acton:
    actor+ main
    ;

main:
    MAIN LCURLY (actor_init)* RCURLY
    ;

actor_init:
    mainActorType=IDENTIFIER mainActorName=IDENTIFIER
    {
        System.out.print("ActorInstantiation:");
        System.out.print($mainActorType.text);
        System.out.print(",");
        System.out.print($mainActorName.text);
    }
    LPAR (arg1Name=IDENTIFIER
    {
        System.out.print(",");
        System.out.print($arg1Name.text);
    }
    (COMMA argnName=IDENTIFIER
    {
        System.out.print(",");
        System.out.print($argnName.text);
    }
    )*
    {
        System.out.print("\n");
    }
    | ) RPAR COLON LPAR ((NUMBER | LITERAL) (COMMA (expr | LITERAL))*|) RPAR SEMICOLON
    ;


actor:
    actor_def LCURLY actor_body RCURLY
    ;

actor_def:
    ACTOR actorDec=IDENTIFIER (EXTENDS actorParent=IDENTIFIER)? {
        System.out.print("ActorDec:");
        System.out.print($actorDec.text);
        if($actorParent != null)
            System.out.print($actorParent.text);
         System.out.print("\n");
    } LPAR NUMBER RPAR
    ;

actor_body:
    known_actors actor_vars msghandlers*
    ;

known_actors:
    KNOWNACTORS LCURLY actor_decl* RCURLY
    ;

actor_decl:
    actorType=IDENTIFIER actorName=IDENTIFIER SEMICOLON
    {
    System.out.print("KnownActor:");
    System.out.print($actorType.text);
    System.out.print(",");
    System.out.print($actorName.text);
    System.out.print("\n");
    }
    ;

actor_vars:
    ACTORVARS LCURLY (var_dcl SEMICOLON)* RCURLY
    ;

types:
    typesRule=(BOOLEAN | INT | STRING)
    {
        System.out.print("VarDec:");
        System.out.print($typesRule.text);
        System.out.print(",");
    }
    ;

array:
    arrType=INT arrName=IDENTIFIER LBRACKET NUMBER RBRACKET
    {
        System.out.print("VarDec:");
        System.out.print($arrType.text);
        System.out.print(",");
        System.out.print($arrName.text);
        System.out.print("\n");
    }
    ;

var_dcl:
    types varName=IDENTIFIER {System.out.print($varName.text); System.out.print("\n");} | array
    ;

msghandlers:
    msghandler_dcl LCURLY msghndlrbody RCURLY
    ;

msghandler_dcl:
    MSGHANDLER msghndlrName=(IDENTIFIER | INITIAL)
    {
    System.out.print("MsgHandlerDec:");
    System.out.print($msghndlrName.text);
    System.out.print("\n");
    }
    LPAR arguments? RPAR
    ;

arguments:
    var_dcl (COMMA var_dcl)*
    ;

body:
    statement body | if_statement body | for_statement body | function_call SEMICOLON | scope |
    ;

for_if_body:
    body | BREAK SEMICOLON | CONTINUE SEMICOLON
    ;

msghndlrbody:
    var_dcl SEMICOLON msghndlrbody | body
    ;

scope:
    LCURLY body RCURLY
    ;

if_statement:
    ifToken=IF
    {
        System.out.print("Conditional:");
        System.out.print($ifToken.text);
        System.out.print("\n");
    }
    LPAR condition RPAR (LCURLY for_if_body RCURLY | single_body) else_statement?
    ;

condition:
    expr | (TRUE | FALSE) | (IDENTIFIER | SENDER) (EQUALS | NOTEQUALS) (IDENTIFIER | SENDER)
    ;

single_body:
    statement | if_statement | for_statement | BREAK SEMICOLON | CONTINUE SEMICOLON | SEMICOLON
    ;

// <---------------- else ---------------->

else_statement:
    elseToken=ELSE
    {
        System.out.print("Conditional:");
        System.out.print($elseToken.text);
        System.out.print("\n");
    }
    (LCURLY for_if_body RCURLY | single_body)
    ;

// <---------------- for ---------------->

for_statement:
    forToken=FOR
    {
        System.out.print("Loop:");
        System.out.print($forToken.text);
        System.out.print("\n");
    }
    LPAR (assignment)? SEMICOLON (expr)? SEMICOLON (assignment)? RPAR (LCURLY for_if_body RCURLY | single_body)
    ;

statement:
    (assignment  | function_call) SEMICOLON
    ;

assignment:
    (IDENTIFIER
    {
        System.out.print("Operator:=");
        System.out.print("\n");
    }
    ASSIGNMENT (expr | strexpr)) | (IDENTIFIER
    {
        System.out.print("Operator:=");
        System.out.print("\n");
    }
    ASSIGNMENT)? (unary_operation)
    ;

unary_operation:
    {
        System.out.print("Operator:++");
        System.out.print("\n");
    }
    INCREMENT IDENTIFIER |
    {
        System.out.print("Operator:--");
        System.out.print("\n");
    }
    DECREMENT IDENTIFIER | IDENTIFIER
    {
        System.out.print("Operator:++");
        System.out.print("\n");
    }
    INCREMENT | IDENTIFIER
    {
        System.out.print("Operator:--");
        System.out.print("\n");
    }
    DECREMENT |
    {
        System.out.print("Operator:!");
        System.out.print("\n");
    }
    NOT IDENTIFIER
    ;

// -------------------- string expr ----------------

strassignment:
    strexpr | IDENTIFIER
    {
        System.out.print("Operator:=");
        System.out.print("\n");
    }
    ASSIGNMENT strexpr
    ;

strexpr:
    LITERAL | LPAR strassignment RPAR
    ;

// -------------------- expr -----------------------

expr:
    assignexpr | IDENTIFIER
    {
        System.out.print("Operator:=");
        System.out.print("\n");
    }
    ASSIGNMENT assignexpr
    ;

assignexpr:
    ternaryexpr | <assoc=right> assignexpr QUESTIONMARK (assignexpr | strassignment) COLON (strassignment | assignexpr)
    ;

ternaryexpr:
    orexpr |
    {
         System.out.print("Operator:||");
         System.out.print("\n");
    }
    orexpr (LOGICALOR orexpr)+
    ;

orexpr:
    andexpr |
    {
        System.out.print("Operator:&&");
        System.out.print("\n");
    }
    andexpr (LOGICALAND andexpr)+
    ;

andexpr:
    equalexpr |
    {
        System.out.print("Operator:==");
        System.out.print("\n");
    }
    equalexpr (EQUALS equalexpr)+ |
    {
        System.out.print("Operator:!=");
        System.out.print("\n");
    }
    equalexpr (NOTEQUALS equalexpr)+
    ;

equalexpr:
    compexpr |
    {
        System.out.print("Operator:<");
        System.out.print("\n");
    }
    compexpr (LESSER compexpr)+ |
    {
        System.out.print("Operator:>");
        System.out.print("\n");
    }
    compexpr (GREATER compexpr)+
    ;

compexpr:
    addsubexpr |
    {
        System.out.print("Operator:+");
        System.out.print("\n");
    }
    addsubexpr (PLUS addsubexpr)+ |
    {
        System.out.print("Operator:-");
        System.out.print("\n");
    }
    addsubexpr (MINUS addsubexpr)+
    ;

addsubexpr:
    multdivmodexpr |
    {
        System.out.print("Operator:*");
        System.out.print("\n");
    }
    multdivmodexpr (MULTIPLICATION multdivmodexpr)+ |
    {
        System.out.print("Operator:/");
        System.out.print("\n");
    }
    multdivmodexpr (DIVIDE multdivmodexpr)+ |
    {
        System.out.print("Operator:%");
        System.out.print("\n");
    }
    multdivmodexpr (MOD multdivmodexpr)+
    ;

multdivmodexpr:
    preexpr | (
    {
        System.out.print("Operator:++");
        System.out.print("\n");
    }
    INCREMENT)+ preexpr | (
    {
        System.out.print("Operator:--");
        System.out.print("\n");
    }
    MINUS)+ preexpr | (
    {
        System.out.print("Operator:!");
        System.out.print("\n");
    }
    NOT)+ preexpr | (
    {
        System.out.print("Operator:--");
        System.out.print("\n");
    }
    DECREMENT)+ preexpr
    ;

preexpr:
    postarrexpr |
    {
        System.out.print("Operator:++");
        System.out.print("\n");
    }
    postarrexpr (INCREMENT (postarrexpr|))+ |
    {
        System.out.print("Operator:--");
        System.out.print("\n");
    }
    postarrexpr (DECREMENT (postarrexpr|))+ | access_array
    ;

postarrexpr:
    atomic | LPAR (atomic|expr) RPAR
    ;

atomic:
    IDENTIFIER | NUMBER | TRUE | FALSE
    ;

access_array:
    IDENTIFIER LBRACKET (NUMBER | expr) RBRACKET;

// ---------------- end of expr ---------------------------------

// ---------------- function call -------------------------------
function_call:
    caller funcName=IDENTIFIER
    {
        System.out.print($funcName.text);
        System.out.print("\n");
    }
    LPAR call_arguments RPAR | printToken=PRINT
    {
        System.out.print("Built-in:Print");
        System.out.print("\n");
    }
    LPAR call_arguments RPAR
    ;

call_arguments:
    (input_arguments | self_argument) (COMMA (input_arguments | self_argument))* |
    ;

self_argument:
    SELF DOT IDENTIFIER (LBRACKET NUMBER RBRACKET)?
    ;

input_arguments:
    expr | LITERAL | IDENTIFIER (LBRACKET NUMBER RBRACKET)?
    ;

caller:
    callerName=(SELF | SENDER | IDENTIFIER)
    {
        System.out.print("MsgHandlerCall:");
        System.out.print($callerName.text);
        System.out.print(",");
    }
    DOT
    ;
